
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//este programa genera los insert necesarios para cargar la tabla programa


public class DimensionPrograma
{
   public static void main(String[] args)
   {
      FachadaBD fachada = new FachadaBD();

      String sql_consultaPrograma = "SELECT * FROM (" +
      "(SELECT codigo_programa, nombre_programa FROM programa_temp) AS a " +
      "NATURAL JOIN (SELECT DISTINCT cod_programa AS codigo_programa, jornada_programa FROM estudiante_temp) AS b) AS c";

      String sql_consultaDirector = "SELECT nombre_docente FROM docente_temp ORDER BY RANDOM() LIMIT 1";

      String sql_consultaEscuela = "SELECT escuela_key FROM escuela ORDER BY RANDOM() LIMIT 1";

      String codigo;
      String nombre;
      String jornada;
      String director;
      String escuela_key;

      try
      {
         Connection conn= fachada.conectar();
         //System.out.println("consultando en la bd");
         Statement sentencia = conn.createStatement();
         Statement sentencia2 = conn.createStatement();
         Statement sentencia3 = conn.createStatement();
         ResultSet tabla = sentencia.executeQuery(sql_consultaPrograma);
         ResultSet tabla2;
         ResultSet tabla3;
            
         while(tabla.next())
         {
            codigo = tabla.getString(1);
            nombre = tabla.getString(2);
            jornada = tabla.getString(3);
            tabla2 = sentencia2.executeQuery(sql_consultaDirector);
            tabla2.next();
            director = tabla2.getString(1);
            tabla3 = sentencia3.executeQuery(sql_consultaEscuela);
            tabla3.next();
            escuela_key = tabla3.getString(1);
            
            System.out.println("INSERT INTO programa VALUES(DEFAULT,'" +
               codigo + "'," +
               num(4) + ",'" + //snies
               nombre + "'," +
               num(3) + "," + //resolucion
               "'Pregrado'" + "," + //nivel
               alea(150,250) + "," + //creditos
               "DEFAULT," + //acreditacion alta calidad
               alea(1960,1990) + "," + //anio_ultimo_registro
               alea(1960,1990) + "," + //anio_inicio_programa
               alea(10,12) + ",'" + //cantidad_semestres
               jornada + "','" + 
               escuela_key + "','" + 
               director +
            "');");
         }
         fachada.cerrarConexion(conn);
      }catch(SQLException e)
      {
         System.out.println(e); 
      }
      catch(Exception e)
      {
         System.out.println(e); 
      }
   }
   
   static String num(int digitos)
   {
       String numd="";
       for(int i=0;i<digitos;i++)
       {
           numd += Integer.toString((int)(Math.random()*9)); 
       }
       return numd;
   }
   
   static String alea(int min, int max)
   {
       String a = Integer.toString((int)(min + (max-min)*Math.random()));
       return a;
   }
}
