--dimension programa
DROP SEQUENCE programa_key_seq;
CREATE SEQUENCE programa_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

DROP TABLE programa;
CREATE TABLE programa
(
   programa_key INTEGER DEFAULT NEXTVAL('programa_key_seq'),
   codigo CHAR(4),
   snies INTEGER,
   nombre VARCHAR(110),
   resolucion INTEGER,
   nivel VARCHAR(10),
   creditos INTEGER,
   acreditacion BOOLEAN DEFAULT TRUE,
   anio_ultimo_registro INTEGER,
   anio_inicio_programa INTEGER,
   cantidad_semestres INTEGER,
   jornada CHAR(3),
   escuela_key CHAR(3),
   director VARCHAR(60)
);
ALTER TABLE programa ADD CONSTRAINT programa_key PRIMARY KEY (programa_key);
ALTER TABLE programa ADD CONSTRAINT escuela_programa_fk FOREIGN KEY (escuela_key) REFERENCES escuela(escuela_key);
