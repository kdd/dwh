package cargadatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class DimensionAsignatura	 {
	
	
     
    
	 
     static int generarNumero(){
    	 
    	 
    	 int numero = (int)(Math.random()*4)+1;
    	 
    	 return numero;
     }
	 
	 public static void main (String args[]){
		
		 FachadaBD fachada;
	     String sql_consultaAsignatura;	    
	     String nombre;
	     String codigo;
	     String creditos;
	     String nivel;
	     String tipo;
	     String escuela_key;
	     HashMap<Integer, String> tipoAsignatura;
	     
	     fachada = new FachadaBD();
    	 sql_consultaAsignatura = " SELECT cod_asignatura,codigo_escuela_ofrece,nombre_asignatura,creditos FROM asignatura_temp ;";
    	 nivel = "Pregrado";
    	 tipoAsignatura = new HashMap<Integer, String>();
    	 tipoAsignatura.put(1,"AB");
    	 tipoAsignatura.put(2,"EC");
    	 tipoAsignatura.put(3,"AP");
    	 tipoAsignatura.put(4,"AE");
	     
	     
		 try
	      {
	         Connection conn= fachada.conectar();
	         
	         Statement sentencia = conn.createStatement();
	         
	         ResultSet tabla = sentencia.executeQuery(sql_consultaAsignatura);
	        
	            
	         while(tabla.next())
	         {
	            codigo = tabla.getString(1);
	            escuela_key = tabla.getString(2);
	            nombre = tabla.getString(3);
	            creditos = tabla.getString(4);
	           
	            
	            System.out.print("INSERT INTO asignatura VALUES(DEFAULT,'" +
	               codigo + "','" +
	               nombre + "'," + 
	               creditos + ",'" + 
	               nivel + "','" + 
	               tipoAsignatura.get(generarNumero()) + "','" +
	               escuela_key +
	            "');");
	         }
	         fachada.cerrarConexion(conn);
	      }catch(SQLException e)
	      {
	         System.out.println(e); 
	      }
	      catch(Exception e)
	      {
	         System.out.println(e); 
	      }
	
	 }

}
