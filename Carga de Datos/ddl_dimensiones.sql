--ddl para la creaacion de las dimensiones

DROP SEQUENCE fecha_key_seq CASCADE;
DROP TABLE fecha CASCADE;
DROP TABLE escuela CASCADE;
DROP SEQUENCE programa_key_seq CASCADE;
DROP TABLE programa CASCADE;
DROP SEQUENCE franja_horaria_key_seq CASCADE;
DROP TABLE franja_horaria CASCADE;
DROP SEQUENCE estudiante_key_seq CASCADE;
DROP TABLE estudiante CASCADE;
DROP SEQUENCE asignatura_key_seq CASCADE;
DROP TABLE asignatura CASCADE;
DROP SEQUENCE demografia_key_seq CASCADE;
DROP TABLE demografia CASCADE;
DROP SEQUENCE equipo_key_seq CASCADE;
DROP TABLE equipo CASCADE;
DROP TABLE matricula CASCADE;
DROP TABLE cancelacion CASCADE;
DROP TABLE agregado_matricula_adicion_cancelacion CASCADE;

--dimension fecha
CREATE SEQUENCE fecha_key_seq
	INCREMENT 1
	MINVALUE 1
	START 1;

CREATE TABLE fecha
(
   fecha_key INTEGER DEFAULT NEXTVAL('fecha_key_seq'),
   numero_dia INTEGER,
   numero_mes INTEGER,
   numero_anio INTEGER,
   nombre_dia VARCHAR(9),
   nombre_mes VARCHAR(10),
   semestre_anio INTEGER,
   periodo_academico VARCHAR(20),
   numero_semana INTEGER
);
ALTER TABLE fecha ADD CONSTRAINT fecha_pk PRIMARY KEY (fecha_key);

--normalizada escuela
CREATE TABLE escuela
(
   escuela_key CHAR(3),
   nombre VARCHAR(60),
   facultad VARCHAR(60),
   sede VARCHAR(20),
   nombre_director VARCHAR(60)
);
ALTER TABLE escuela ADD CONSTRAINT escuela_pk PRIMARY KEY (escuela_key);

--dimension programa
CREATE SEQUENCE programa_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

CREATE TABLE programa
(
   programa_key INTEGER DEFAULT NEXTVAL('programa_key_seq'),
   codigo CHAR(4),
   snies INTEGER,
   nombre VARCHAR(110),
   resolucion INTEGER,
   nivel VARCHAR(10),
   creditos INTEGER,
   acreditacion BOOLEAN DEFAULT TRUE,
   anio_ultimo_registro INTEGER,
   anio_inicio_programa INTEGER,
   cantidad_semestres INTEGER,
   jornada CHAR(3),
   escuela_key CHAR(3),
   director VARCHAR(60)
);
ALTER TABLE programa ADD CONSTRAINT programa_key PRIMARY KEY (programa_key);
ALTER TABLE programa ADD CONSTRAINT escuela_programa_fk FOREIGN KEY (escuela_key) REFERENCES escuela(escuela_key);

--dimension franaja_horaria
CREATE SEQUENCE franja_horaria_key_seq
	INCREMENT 1
	MINVALUE 1
	START 1;

CREATE TABLE franja_horaria
(
   franja_horaria_key INTEGER  NOT NULL DEFAULT nextval('franja_horaria_key_seq'), 
   hora_inicio  TIME, 
   hora_fin    TIME , 
   nombre_franja   varchar(10) ,
   PRIMARY KEY (franja_horaria_key)
);

--dimension estudiante
CREATE SEQUENCE estudiante_key_seq
   INCREMENT 1
   MINVALUE 1
   START 1;

CREATE TABLE estudiante
(
   estudiante_key INTEGER NOT NULL DEFAULT nextval('estudiante_key_seq'),
   codigo CHAR(7),
   nombre VARCHAR(15),
   apellido VARCHAR(15),
   apellido2 VARCHAR(15),
   documento_identidad VARCHAR(10),
   telefono VARCHAR(7),
   direccion_residencia VARCHAR(50),
   genero CHAR(1),
   fecha_nacimiento DATE,
   correo_electronico VARCHAR(50),
   colegio VARCHAR(80),
   año_graduacion INTEGER,
   validar_grado_flag BOOLEAN DEFAULT FALSE,
   categoria VARCHAR(10) DEFAULT 'Regular',
   icfes_SNP VARCHAR(14),
   icfes_tipo VARCHAR(20),
   icfes_biologia INTEGER,
   icfes_fisica INTEGER,
   icfes_lenguaje INTEGER,
   icfes_matematicas INTEGER,
   icfes_filosofia INTEGER,
   icfes_quimica INTEGER,
   icfes_geografia INTEGER,
   icfes_historia INTEGER,
   icfes_idioma INTEGER,
   icfes_interdisciplinar INTEGER
);
ALTER TABLE estudiante ADD CONSTRAINT estudiante_pk PRIMARY KEY (estudiante_key);

--dimension asignatura
CREATE SEQUENCE asignatura_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

CREATE TABLE asignatura
(
   asignatura_key INTEGER DEFAULT NEXTVAL('asignatura_key_seq'),
   codigo CHAR(10),
   nombre VARCHAR(110),
   creditos INTEGER,
   nivel VARCHAR(10),
   tipo CHAR(2),
   escuela_key CHAR(3)   
);
ALTER TABLE asignatura ADD CONSTRAINT asignatura_pk PRIMARY KEY (asignatura_key);
ALTER TABLE asignatura ADD CONSTRAINT escuela_asignatura_fk FOREIGN KEY (escuela_key) REFERENCES escuela(escuela_key);

--dimension demografia
CREATE SEQUENCE demografia_key_seq
   INCREMENT 1
   MINVALUE 1
   START 1;

CREATE TABLE demografia
(
   demografia_key INTEGER DEFAULT NEXTVAL('demografia_key_seq'),
   tipo_documento_identidad CHAR(2),
   estado_civil VARCHAR(13),
   ciudad_residencia VARCHAR(30),
   barrio VARCHAR(40),
   comuna VARCHAR(2),
   estrato INTEGER,
   numero_hermanos INTEGER
);
ALTER TABLE demografia ADD CONSTRAINT demografia_pk PRIMARY KEY (demografia_key);

--dimension equipo
CREATE SEQUENCE equipo_key_seq 
   INCREMENT 1 
   MINVALUE 1  
   START 1;

CREATE TABLE equipo
(
   equipo_key INTEGER DEFAULT NEXTVAL('equipo_key_seq'),
   mac CHAR(17),
   ip VARCHAR(15),
   espacio CHAR(5),
   edificio CHAR(4),
   marca VARCHAR(10),
   modelo VARCHAR(100),
   inventario VARCHAR(10),
   capacidad_ram VARCHAR(10),
   capacidad_disco VARCHAR(10),
   procesador VARCHAR(50),
   tarjeta_video VARCHAR(50),
   sistema_operativo VARCHAR(20)
 
   
);
ALTER TABLE equipo ADD CONSTRAINT equipo_key PRIMARY KEY (equipo_key);

CREATE TABLE matricula
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   equipo_key INTEGER,
   estudiante_key INTEGER,
   demografia_key INTEGER,
   franja_horaria_key INTEGER,
   tabulado_key INTEGER,
   grupo INTEGER
);

CREATE TABLE cancelacion
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   equipo_key INTEGER,
   estudiante_key INTEGER,
   demografia_key INTEGER,
   franja_horaria_key INTEGER,
   tabulado_key INTEGER,
   grupo INTEGER
);
ALTER TABLE cancelacion ADD CONSTRAINT cancelacion_key PRIMARY KEY(fecha_key,asignatura_key,programa_key,equipo_key,estudiante_key,demografia_key,franja_horaria_key,grupo);

CREATE TABLE agregado_matricula_adicion_cancelacion
(
   fecha_key INTEGER,
   asignatura_key INTEGER,
   programa_key INTEGER,
   franja_horaria_key INTEGER,
   total_matriculas INTEGER,
   total_cancelaciones INTEGER,
   total_adiciones INTEGER
);
ALTER TABLE agregado_matricula_adicion_cancelacion ADD CONSTRAINT agregado_mac_pk PRIMARY KEY (fecha_key,asignatura_key,programa_key,franja_horaria_key);
