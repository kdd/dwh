--dimension demografia

DROP SEQUENCE demografia_key_seq;
CREATE SEQUENCE demografia_key_seq
   INCREMENT 1
   MINVALUE 1
   START 1;

DROP TABLE demografia;
CREATE TABLE demografia
(
   demografia_key INTEGER DEFAULT NEXTVAL('demografia_key_seq'),
   tipo_documento_identidad CHAR(2),
   estado_civil VARCHAR(13),
   ciudad_residencia VARCHAR(30),
   barrio VARCHAR(40),
   comuna VARCHAR(2),
   estrato INTEGER,
   numero_hermanos INTEGER
);
ALTER TABLE demografia ADD CONSTRAINT demografia_pk PRIMARY KEY (demografia_key);
