DROP TABLE escuela;
CREATE TABLE escuela
(
   escuela_key CHAR(3),
   nombre VARCHAR(60),
   facultad VARCHAR(60),
   sede VARCHAR(20),
   nombre_director VARCHAR(60)
);
ALTER TABLE escuela ADD CONSTRAINT escuela_pk PRIMARY KEY (escuela_key);
