--la sigueinte consulta genera los insert que se deben de ejecutar para poblar de datos la tabla escuela

--este archivo debe de ejecutarse de la siguiente manera: 
--psql -U usuario -d base -h servidor < ScriptGeneracionEscuelas.sql > ScriptEscuela.sql

--luego se debebe ejecutar nuevamente el archivo generado de la siguiente manera:
--psql -U usuario -d base -h servidor < ScriptEscuela.sql


SELECT 'INSERT INTO escuela VALUES( ''' ||  e.codigo || ''' , ''' ||  e.nombre  || ''' , ''' || e.facultad  || ''','''|| 'Cali-Melendez' || ''' , ''' || (select nombre_docente from docente_temp natural join (select cast(e.total*random() as integer)  as id) as h )|| ''');' FROM (SELECT DISTINCT codigo_escuela_ofrece AS codigo, nombre_escuela_ofrece AS nombre, nombre_facultad AS facultad,  (SELECT COUNT(*) FROM docente_temp) as total FROM asignatura_temp ORDER BY nombre) AS e;
