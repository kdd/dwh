package cargadatos;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Estrellas {
  
  static String cantidadTuplasConEstosAtributos(String programa,
      String asignatura, String fecha, String franja_horaria) {

    String sql_existeTuplaAgregado = "SELECT COUNT(*) FROM agregado_matricula_adicion_cancelacion WHERE programa_key = "
        + programa
        + ""
        + " AND asignatura_key = "
        + asignatura
        + " AND fecha_key = "
        + fecha
        + " AND franja_horaria_key = "
        + franja_horaria + ";";

    
    return sql_existeTuplaAgregado;
  }

  static String keyAsignatura(String codigo) {
    String sql_consultaAsignatura = "SELECT asignatura_key FROM asignatura WHERE codigo = '"
        + codigo + "'";
    return sql_consultaAsignatura;
  }

  static String keyEstudiante(String codigo) {
    String sql_consultaEstudiante = "SELECT estudiante_key FROM estudiante WHERE codigo = '"
        + codigo + "'";
    return sql_consultaEstudiante;
  }

  static String keyFecha(String fecha) {
    String c[] = fecha.split("-");
    String dia = c[2];
    String mes = c[1];
    String anio = c[0];

    String sql_consultaFecha = "SELECT fecha_key FROM fecha WHERE numero_dia = "
        + dia
        + " AND numero_mes = "
        + mes
        + " AND numero_año = "
        + anio;

    return sql_consultaFecha;
  }

  static String keyFranjaHoraria(String tiempo) {
    String sql_consultaFranja = "SELECT franja_horaria_key FROM franja_horaria WHERE '"
        + tiempo + "' BETWEEN hora_inicio AND hora_fin";
    return sql_consultaFranja;
  }

  static String keyPrograma(String codigo) {
    String sql_consultaPrograma = "SELECT programa_key FROM programa WHERE codigo = '"
        + codigo + "'";
    return sql_consultaPrograma;
  }

  public static void main(String args[]) throws IOException {
  
    StringBuilder contenedorMatricula = new StringBuilder();
    StringBuilder contenedorCancelacion = new StringBuilder();
    StringBuilder contenedorAgregado = new StringBuilder();
    

    
    FachadaBD fachada = new FachadaBD();

    String sql_consultaMatricula = "SELECT cod_estudiante, cod_prog_academico, "
        + "cod_asignatura, grupo,fecha_matricula::date AS date_matricula, "
        + "fecha_matricula::time AS time_matricula, "
        + "COALESCE(fecha_cancelacion::date,'0001-01-01') AS date_cancelacion, "
        + "COALESCE(fecha_cancelacion::time,'00:00:00') AS time_cancelacion, "
        + "COALESCE(fecha_reactivacion::date, '0001-01-01') AS date_reactivacion, "
        + "COALESCE(fecha_reactivacion::time, '00:00:00') As time_reactivacion "
        + "FROM matricula_temp";

    String sql_consultaDemografia = "SELECT demografia_key FROM demografia ORDER BY RANDOM() LIMIT 1";
    String sql_consultaEquipo = "SELECT equipo_key FROM equipo ORDER BY RANDOM() LIMIT 1";

    try {
      Connection conn = fachada.conectar();

      Statement sentenciaMatricula = conn.createStatement();
      Statement sentenciaPrograma = conn.createStatement();
      Statement sentenciaAsignatura = conn.createStatement();
      Statement sentenciaFecha = conn.createStatement();
      Statement sentenciaFranja = conn.createStatement();
      Statement sentenciaEquipo = conn.createStatement();
      Statement sentenciaEstudiante = conn.createStatement();
      Statement sentenciaDemografia = conn.createStatement();
      Statement sentenciaAgregado = conn.createStatement();
      Statement sentenciaActualizacion = conn.createStatement();

      ResultSet tablaMatricula = sentenciaMatricula.executeQuery(sql_consultaMatricula);
      ResultSet tablaPrograma;
      ResultSet tablaAsignatura;
      ResultSet tablaFecha;
      ResultSet tablaEquipo;
      ResultSet tablaEstudiante;
      ResultSet tablaDemografia;
      ResultSet tablaFranja;
      ResultSet tablaAgregado;

      String estudiante;
      String programa;
      String asignatura;
      String franja;
      String equipo;
      String demografia;
      String fecha;
      String grupo;
      String cod_estudiante;
      String acierto;

      while (tablaMatricula.next()) {
        grupo = tablaMatricula.getString("grupo");

        cod_estudiante = tablaMatricula.getString("cod_estudiante");
        tablaEstudiante = sentenciaEstudiante.executeQuery(keyEstudiante(cod_estudiante));
        tablaEstudiante.next();
        estudiante = tablaEstudiante.getString(1);
      
        tablaPrograma = sentenciaPrograma.executeQuery(keyPrograma(tablaMatricula.getString("cod_prog_academico")));
        tablaPrograma.next();
        programa = tablaPrograma.getString(1);
        
        tablaAsignatura = sentenciaAsignatura.executeQuery(keyAsignatura(tablaMatricula.getString("cod_asignatura")));
        tablaAsignatura.next();
        asignatura = tablaAsignatura.getString(1);
        
        tablaFecha = sentenciaFecha.executeQuery(keyFecha(tablaMatricula.getString("date_matricula")));
        tablaFecha.next();
        fecha = tablaFecha.getString(1);
              

        tablaFranja = sentenciaFranja.executeQuery(keyFranjaHoraria(tablaMatricula.getString("time_matricula")));
        tablaFranja.next();
        franja = tablaFranja.getString(1);        

        tablaEquipo = sentenciaEquipo.executeQuery(sql_consultaEquipo);
        tablaEquipo.next();
        equipo = tablaEquipo.getString(1);        

        tablaDemografia = sentenciaDemografia.executeQuery(sql_consultaDemografia);
        tablaDemografia.next();
        demografia = tablaDemografia.getString(1);
        
        /*
        sentenciaActualizacion.executeUpdate("INSERT INTO matricula VALUES(" + fecha
                + "," + asignatura + "," + programa + ","
                + equipo + "," + estudiante + "," + demografia
                + "," + franja + "," + cod_estudiante + "112"
                + "," + grupo + ");\n");
                
                */
        
        contenedorMatricula.append("INSERT INTO matricula VALUES(" + fecha
            + "," + asignatura + "," + programa + ","
            + equipo + "," + estudiante + "," + demografia
            + "," + franja + "," + cod_estudiante + "112"
            + "," + grupo + ");\n");

        tablaAgregado = sentenciaAgregado.executeQuery(cantidadTuplasConEstosAtributos(programa,asignatura, fecha, franja));

        tablaAgregado.next();
        acierto = tablaAgregado.getString(1);

        // No hay ocurrencias de este programa matriculando esta asignatura en esta fecha en esta franja horaria
        // Por lo tanto se añade una tupla a la tabla de agregado
        if (acierto.equals("0")) {
          sentenciaActualizacion
              .executeUpdate("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                  + fecha
                  + ","
                  + asignatura
                  + ","
                  + programa
                  + ","
                  + franja
                  + ","
                  + "1 ,"
                  + "0 ,"
                  + "0"
                  + ");");
          
          contenedorAgregado.append("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                  + fecha
                  + ","
                  + asignatura
                  + ","
                  + programa
                  + ","
                  + franja
                  + ","
                  + "1 ,"
                  + "0 ,"
                  + "0"
                  + ");\n");
          

          // Si hay ocurrencias de este programa matriculando esta asignatura en esta fecha en esta franja horaria
        // Por lo tanto se una actualizacion con respecto al atributo de total matriculas.
        } else {

          String cantidadMatriculas;
          tablaAgregado = sentenciaAgregado
              .executeQuery(tuplaAgregado(programa, asignatura,
                  fecha, franja));
          tablaAgregado.next();
          cantidadMatriculas = tablaAgregado.getString(5);

          int totalMatriculas = Integer.parseInt(cantidadMatriculas);
          totalMatriculas += 1;

          sentenciaActualizacion.executeUpdate("UPDATE agregado_matricula_adicion_cancelacion SET "
                  + "total_matriculas = "
                  + Integer.toString(totalMatriculas)
                  + " WHERE "
                  + "asignatura_key = "
                  + asignatura
                  + " AND "
                  + "programa_key = "
                  + programa
                  + " AND "
                  + "fecha_key = "
                  + fecha
                  + " AND "
                  + "franja_horaria_key = "
                  + franja + ";");
          
          contenedorAgregado.append("UPDATE agregado_matricula_adicion_cancelacion SET "
                  + "total_matriculas = "
                  + Integer.toString(totalMatriculas)
                  + " WHERE "
                  + "asignatura_key = "
                  + asignatura
                  + " AND "
                  + "programa_key = "
                  + programa
                  + " AND "
                  + "fecha_key = "
                  + fecha
                  + " AND "
                  + "franja_horaria_key = "
                  + franja + ";\n");
          

        }
        

        fecha = tablaMatricula.getString("date_cancelacion");

        if (!fecha.equals("0001-01-01")) {
          tablaFecha = sentenciaFecha.executeQuery(keyFecha(fecha));
          tablaFecha.next();
          fecha = tablaFecha.getString(1);

          tablaFranja = sentenciaFranja.executeQuery(keyFranjaHoraria(tablaMatricula.getString("time_cancelacion")));
          tablaFranja.next();
          franja = tablaFranja.getString(1);

          /*sentenciaActualizacion.executeUpdate("INSERT INTO cancelacion VALUES("
                  + fecha + "," + asignatura + "," + programa
                  + "," + equipo + "," + estudiante + ","
                  + demografia + "," + franja + ","
                  + cod_estudiante + "112" + "," + grupo
                  + ");\n");*/
          contenedorCancelacion.append("INSERT INTO cancelacion VALUES("
                  + fecha + "," + asignatura + "," + programa
                  + "," + equipo + "," + estudiante + ","
                  + demografia + "," + franja + ","
                  + cod_estudiante + "112" + "," + grupo
                  + ");\n");

          tablaAgregado = sentenciaAgregado.executeQuery(cantidadTuplasConEstosAtributos(
                  programa, asignatura, fecha, franja));

          tablaAgregado.next();
          acierto = tablaAgregado.getString(1);

          if (acierto.equals("0")) {
            sentenciaActualizacion
                .executeUpdate("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                    + fecha
                    + ","
                    + asignatura
                    + ","
                    + programa
                    + ","
                    + franja
                    + ","
                    + "0 ,"
                    + "1 ," + "0 " + ");");
            contenedorAgregado.append("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                    + fecha
                    + ","
                    + asignatura
                    + ","
                    + programa
                    + ","
                    + franja
                    + ","
                    + "0 ,"
                    + "1 ," + "0 " + ");\n");

          } else {

            String cantidadCancelaciones;
            tablaAgregado = sentenciaAgregado
                .executeQuery(tuplaAgregado(programa,
                    asignatura, fecha, franja));
            tablaAgregado.next();
            cantidadCancelaciones = tablaAgregado.getString(6);

            int totalCancelaciones = Integer
                .parseInt(cantidadCancelaciones);
            totalCancelaciones += 1;

            sentenciaActualizacion.executeUpdate("UPDATE agregado_matricula_adicion_cancelacion SET "
                    + "total_cancelaciones = "
                    + Integer.toString(totalCancelaciones)
                    + " WHERE "
                    + "asignatura_key = "
                    + asignatura
                    + " AND "
                    + "programa_key = "
                    + programa
                    + " AND "
                    + "fecha_key = "
                    + fecha
                    + " AND "
                    + "franja_horaria_key = "
                    + franja + ";");
            
            contenedorAgregado.append("UPDATE agregado_matricula_adicion_cancelacion SET "
                    + "total_cancelaciones = "
                    + Integer.toString(totalCancelaciones)
                    + " WHERE "
                    + "asignatura_key = "
                    + asignatura
                    + " AND "
                    + "programa_key = "
                    + programa
                    + " AND "
                    + "fecha_key = "
                    + fecha
                    + " AND "
                    + "franja_horaria_key = "
                    + franja + ";\n");

          }
        }

        
        fecha = tablaMatricula.getString("date_reactivacion");

        if (!fecha.equals("0001-01-01")) {
          tablaFecha = sentenciaFecha.executeQuery(keyFecha(fecha));
          tablaFecha.next();
          fecha = tablaFecha.getString(1);

          tablaFranja = sentenciaFranja
              .executeQuery(keyFranjaHoraria(tablaMatricula
                  .getString("time_reactivacion")));
          tablaFranja.next();
          franja = tablaFranja.getString(1);

          /*sentenciaActualizacion.executeUpdate("INSERT INTO matricula VALUES("
                  + fecha + "," + asignatura + "," + programa
                  + "," + equipo + "," + estudiante + ","
                  + demografia + "," + franja + ","
                  + cod_estudiante + "112" + "," + grupo
                  + ");\n");*/
          
          contenedorMatricula.append("INSERT INTO matricula VALUES("
              + fecha + "," + asignatura + "," + programa
              + "," + equipo + "," + estudiante + ","
              + demografia + "," + franja + ","
              + cod_estudiante + "112" + "," + grupo
              + ");\n");

          tablaAgregado = sentenciaAgregado.executeQuery(cantidadTuplasConEstosAtributos(
                  programa, asignatura, fecha, franja));

          tablaAgregado.next();
          acierto = tablaAgregado.getString(1);

          if (acierto.equals("0")) {
            sentenciaActualizacion
                .executeUpdate("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                    + fecha
                    + ","
                    + asignatura
                    + ","
                    + programa
                    + ","
                    + franja
                    + ","
                    + "1 ,"
                    + "0 ," + "0 " + ");");
            
            contenedorAgregado.append("INSERT INTO agregado_matricula_adicion_cancelacion VALUES("
                    + fecha
                    + ","
                    + asignatura
                    + ","
                    + programa
                    + ","
                    + franja
                    + ","
                    + "1 ,"
                    + "0 ," + "0 " + ");\n");

          } else {

            String cantidadMatriculas;
            tablaAgregado = sentenciaAgregado
                .executeQuery(tuplaAgregado(programa,
                    asignatura, fecha, franja));
            tablaAgregado.next();
            cantidadMatriculas = tablaAgregado.getString(5);

            int totalMatriculas = Integer
                .parseInt(cantidadMatriculas);
            totalMatriculas += 1;

            sentenciaActualizacion
                .executeUpdate("UPDATE agregado_matricula_adicion_cancelacion SET "
                    + "total_matriculas = "
                    + Integer.toString(totalMatriculas)
                    + " WHERE "
                    + "asignatura_key = "
                    + asignatura
                    + " AND "
                    + "programa_key = "
                    + programa
                    + " AND "
                    + "fecha_key = "
                    + fecha
                    + " AND "
                    + "franja_horaria_key = "
                    + franja + ";");
            
            contenedorAgregado.append("UPDATE agregado_matricula_adicion_cancelacion SET "
                    + "total_matriculas = "
                    + Integer.toString(totalMatriculas)
                    + " WHERE "
                    + "asignatura_key = "
                    + asignatura
                    + " AND "
                    + "programa_key = "
                    + programa
                    + " AND "
                    + "fecha_key = "
                    + fecha
                    + " AND "
                    + "franja_horaria_key = "
                    + franja + ";\n");

          }
        }
        

        
      }
        /*archivos para guardar los inserts*/
          BufferedWriter fileMatricula = null;
          BufferedWriter fileCancelacion = null;
          BufferedWriter fileAgregado = null;
          
          try
          {    
            
              fileMatricula = new BufferedWriter(new PrintWriter(new FileWriter("ScriptMatricula.sql")));
              fileCancelacion = new BufferedWriter(new PrintWriter(new FileWriter("ScriptCancelacion.sql")));
              fileAgregado = new BufferedWriter(new PrintWriter(new FileWriter("ScriptAgregado.sql")));
          } catch (IOException ex) {
             
          }
          
         
          fileMatricula.write(contenedorMatricula.toString());
          fileCancelacion.write(contenedorCancelacion.toString());
          fileAgregado.write(contenedorAgregado.toString());
          
      fachada.cerrarConexion(conn);
      
      fileMatricula.close();
          fileCancelacion.close();
          fileAgregado.close();

    } catch (SQLException e) {
      
      System.out.println(e);
    } catch (Exception e) {

      System.out.println(e);
    }

  }

  static String tuplaAgregado(String programa, String asignatura,
      String fecha, String franja_horaria) {

    String sql_TuplaAgregado = "SELECT * FROM agregado_matricula_adicion_cancelacion WHERE programa_key = "
        + programa
        + ""
        + " AND asignatura_key = "
        + asignatura
        + " AND fecha_key = "
        + fecha
        + " AND franja_horaria_key = "
        + franja_horaria + ";";

    
    return sql_TuplaAgregado;
  }
}
