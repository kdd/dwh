
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Estrellas 
{
    public static void main(String args[])
    {
        /*archivos para guardar los inserts*/
        BufferedWriter fileMatricula = null;
        BufferedWriter fileCancelacion = null;
        
        try
        {    
          
            fileMatricula = new BufferedWriter(new PrintWriter(new FileWriter("ScriptMatricula.txt")));
            fileCancelacion = new BufferedWriter(new PrintWriter(new FileWriter("ScriptCancelacion.txt")));
        } catch (IOException ex) {
            Logger.getLogger(Estrellas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        FachadaBD fachada = new FachadaBD();
        
        String sql_consultaMatricula = "SELECT cod_estudiante, cod_prog_academico, "
                + "cod_asignatura, grupo, grupo, fecha_matricula::date AS date_matricula, "
                + "fecha_matricula::time AS time_matricula, "
                + "COALESCE(fecha_cancelacion::date,'0001-01-01') AS date_cancelacion, "
                + "COALESCE(fecha_cancelacion::time,'00:00:00') AS time_cancelacion, "
                + "COALESCE(fecha_reactivacion::date, '0001-01-01') AS date_reactivacion, "
                + "COALESCE(fecha_reactivacion::time, '00:00:00') As time_reactivacion "
                + "FROM matricula_temp";
        
        String sql_consultaDemografia = "SELECT demografia_key FROM demografia ORDER BY RANDOM() LIMIT 1";
        String sql_consultaEquipo = "SELECT equipo_key FROM equipo ORDER BY RANDOM() LIMIT 1";
        
         try
      {
         Connection conn= fachada.conectar();
         
         Statement sentenciaMatricula = conn.createStatement();
         Statement sentenciaPrograma = conn.createStatement();
         Statement sentenciaAsignatura = conn.createStatement();
         Statement sentenciaFecha = conn.createStatement();
         Statement sentenciaFranja =conn.createStatement();
         Statement sentenciaEquipo = conn.createStatement();
         Statement sentenciaEstudiante = conn.createStatement();
         Statement sentenciaDemografia = conn.createStatement();
         
         ResultSet tablaMatricula = sentenciaMatricula.executeQuery(sql_consultaMatricula);
         ResultSet tablaPrograma;
         ResultSet tablaAsignatura;
         ResultSet tablaFecha;
         ResultSet tablaEquipo;
         ResultSet tablaEstudiante;
         ResultSet tablaDemografia;
         ResultSet tablaFranja;
         
         String estudiante;
         String programa;
         String asignatura;
         String franja;
         String equipo;
         String demografia;
         String fecha;
         String grupo;
         String cod_estudiante;

            
         while(tablaMatricula.next())
         {
             grupo = tablaMatricula.getString("grupo");
                //System.out.println("grupo " + grupo);
             cod_estudiante = tablaMatricula.getString("cod_estudiante");
             tablaEstudiante = sentenciaEstudiante.executeQuery(keyEstudiante(cod_estudiante));
             tablaEstudiante.next();
             estudiante = tablaEstudiante.getString(1);
                //System.out.println("estudiante " + estudiante);
             tablaPrograma = sentenciaPrograma.executeQuery(keyPrograma(tablaMatricula.getString("cod_prog_academico")));
             tablaPrograma.next();
             programa = tablaPrograma.getString(1);
                //System.out.println("programa " + programa);
             tablaAsignatura = sentenciaAsignatura.executeQuery(keyAsignatura(tablaMatricula.getString("cod_asignatura")));
             tablaAsignatura.next();
             asignatura = tablaAsignatura.getString(1);
                //System.out.println("asignatura " + asignatura);
             tablaFecha = sentenciaFecha.executeQuery(keyFecha(tablaMatricula.getString("date_matricula")));
             tablaFecha.next();
             fecha = tablaFecha.getString(1);
                //System.out.println("fecha " + fecha);
             tablaFranja = sentenciaFranja.executeQuery(keyFranjaHoraria(tablaMatricula.getString("time_matricula")));
             tablaFranja.next();
             franja = tablaFranja.getString(1);
                //System.out.println("franja " + franja);
             tablaEquipo = sentenciaEquipo.executeQuery(sql_consultaEquipo);
             tablaEquipo.next();
             equipo = tablaEquipo.getString(1);
                //System.out.println("equipo " + equipo);
             tablaDemografia = sentenciaDemografia.executeQuery(sql_consultaDemografia);
             tablaDemografia.next();
             demografia = tablaDemografia.getString(1);
                //System.out.println("demografia " + demografia);
             fileMatricula.write("INSERT INTO matricula VALUES("
                     + fecha + ","
                     + asignatura + ","
                     + programa + ","
                     + equipo + ","
                     + estudiante + ","
                     + demografia + ","
                     + franja + ","
                     + cod_estudiante + "112" + "," //codigo del estudiante seguido del año seguido del semestre
                     + grupo + ");\n");
             
             fecha = tablaMatricula.getString("date_cancelacion");
             if(!fecha.equals("0001-01-01"))
             {
                 tablaFecha = sentenciaFecha.executeQuery(keyFecha(fecha));
                 tablaFecha.next();
                 fecha = tablaFecha.getString(1);
                 
                 tablaFranja = sentenciaFranja.executeQuery(keyFranjaHoraria(tablaMatricula.getString("time_cancelacion")));
                 tablaFranja.next();
                 franja = tablaFranja.getString(1);
                 
                 fileCancelacion.write("INSERT INTO cancelacion VALUES("
                     + fecha + ","
                     + asignatura + ","
                     + programa + ","
                     + equipo + ","
                     + estudiante + ","
                     + demografia + ","
                     + franja + ","
                     + cod_estudiante + "112" + "," //codigo del estudiante seguido del año seguido del semestre
                     + grupo +");\n");
             }
                
             fecha = tablaMatricula.getString("date_reactivacion");
             if(!fecha.equals("0001-01-01"))
             {
                 tablaFecha = sentenciaFecha.executeQuery(keyFecha(fecha));
                 tablaFecha.next();
                 fecha = tablaFecha.getString(1);
                 
                 tablaFranja = sentenciaFranja.executeQuery(keyFranjaHoraria(tablaMatricula.getString("time_reactivacion")));
                 tablaFranja.next();
                 franja = tablaFranja.getString(1);
                 
                 fileMatricula.write("INSERT INTO matricula VALUES("
                     + fecha + ","
                     + asignatura + ","
                     + programa + ","
                     + equipo + ","
                     + estudiante + ","
                     + demografia + ","
                     + franja + ","
                     + cod_estudiante + "112" + "," //codigo del estudiante seguido del año seguido del semestre
                     + grupo +");\n");
             }            
         }
         
         fachada.cerrarConexion(conn);
         fileMatricula.close();
         fileCancelacion.close();
      }catch(SQLException e)
      {
         System.out.println(e); 
      }
      catch(Exception e)
      {
         System.out.println(e); 
      }
     
    }
    
    static String keyPrograma(String codigo)
    {
        String sql_consultaPrograma = "SELECT programa_key FROM programa WHERE codigo = '"+codigo+"'";
        return sql_consultaPrograma;
    }
    
    static String keyFecha(String fecha)       
    {
        String c[] = fecha.split("-");
        String dia = c[2];
        String mes = c[1];
        String anio =c[0];
        
        String sql_consultaFecha = "SELECT fecha_key FROM fecha WHERE numero_dia = " + dia + " AND numero_mes = " + mes +
                " AND numero_anio = " + anio;
        
        return sql_consultaFecha;
    }
    
    static String keyFranjaHoraria(String tiempo)
    {
        String sql_consultaFranja = "SELECT franja_horaria_key FROM franja_horaria WHERE '" +tiempo+"' BETWEEN hora_inicio AND hora_fin";
        return sql_consultaFranja;
    }
    
    static String keyEstudiante(String codigo)
    {
        String sql_consultaEstudiante = "SELECT estudiante_key FROM estudiante WHERE codigo = '" + codigo + "'";
        return sql_consultaEstudiante;
    }
    
    static String keyAsignatura(String codigo)
    {
        String sql_consultaAsignatura = "SELECT asignatura_key FROM asignatura WHERE codigo = '" +codigo + "'";
        return sql_consultaAsignatura;
    }
}
