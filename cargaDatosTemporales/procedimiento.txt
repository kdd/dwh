procedimiento seguido para cargar las tablas temporales de datos

1. ejecutar el escrip ddl.sql
2. copiar lo archivos que tiene los datos a cargar en la carpeta /tmp
3. dar a los archivos que tiene los datos todos los permisos, esto es ejecutar chmod 777
4. conectarse a la base de datos con un usuario administrador para que puede ejecutar el comando copy
5. ejecutar lo siguiente:

   COPY docente_temp FROM '/tmp/bd_docentes.txt' WITH DELIMITER '*' NULL AS '';
   COPY asignatura_temp FROM '/tmp/bd_asignaturas.txt' WITH DELIMITER '*' NULL AS '';
   COPY programa_temp FROM '/tmp/bd_programas.txt' WITH DELIMITER '*' NULL AS '';
   COPY estudiante_temp FROM '/tmp/bd_estudiantes.txt' WITH DELIMITER '*' NULL AS '';
   COPY matricula_temp FROM '/tmp/bd_matricula1.txt' WITH DELIMITER '*' NULL AS '';
   COPY matricula_temp FROM '/tmp/bd_matricula2.txt' WITH DELIMITER '*' NULL AS '';
   COPY matricula_temp FROM '/tmp/bd_matricula3.txt' WITH DELIMITER '*' NULL AS '';
   COPY matricula_temp FROM '/tmp/bd_matricula4.txt' WITH DELIMITER '*' NULL AS '';
   COPY email_temp FROM '/tmp/emails.txt' WITH DELIMITER '*' NULL AS '';
   COPY colegio_temp FROM '/tmp/colegiosCali.txt' WITH DELIMITER '*' NULL AS '';
   COPY barrio_temp FROM '/tmp/barriosCali.txt' WITH DELIMITER '*' NULL AS '';

al finalizar cada carga se debe verificar que la cantidad de registros que indica que se han cargado sea la misma cantidad de lineas del archivo que se cargó. Se dividio matricula en 4 archivos por su manejo, además en la carga se estaban perdiendo registros, me imagino que por la cantidad.

NOTA: No se si sea bueno que la base se llame igual para todos, en mi caso la base se llama kdd y usuario y el pass es clrl
los barrios fueron tomados de planeacion.cali.gov.co 
los colegios fueron tomados de www.colegioscolombia.com
los emails fueron tomados de http://rapidshare.com/files/89767184/emails.txt
